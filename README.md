# lea-latex-beamer-template

This is a LaTeX beamer document template for LEA students, writing a project report or a thesis.

## Prerequisites
This beamer template is based on the [metropolis](https://github.com/matze/mtheme) theme.
Therefore it must be installed first if it is not already included in your LaTeX distribution.

## Using the template
Run 'presentation.tex' using an LaTeX editor, e.g. TeXstudio.

## Overleaf
Set the main document :
`Menu` -> `Main Document` -> `presentation.tex`

## TeXstudio
Change the compiler to `Latexmk`
`Options` -> `Configure TeXstudio` -> `Generate` -> default compiler: `Latexmk`

## Preview
![](preview-0.png)
![](preview-2.png)
![](preview-3.png)
